const express = require('express');
const mysql = require('mysql');
const myconn = require('express-myconnection');
const routes = require('./router');

const app = express();
app.set('port', process.env.PORT || 8000)
app.use(express.urlencoded({extended:false}));
app.use(express.json());

const dbOptions = {
    host ="localhost",
    port = 3306,
    user = "root",
    password = "",
    database = "attack"
};

app.use(myconn(mysql,dbOptions, 'single'));

app.use('/api/resources', routes)

app.listen(app.get('port'), ()=>{
    console.log("El servidor esta desplegado en el puerto", app.get('port'));
});