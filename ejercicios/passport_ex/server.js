const express = require('express');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const PassportLocal = require('passport-local');

const app = express();

//middleware

app.use(express.urlencoded({extended:true}));
app.use(cookieParser('secretkey'));
app.use(session({
    secret: 'secretkey',
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new PassportLocal(function(username,password,done){
    if(username === 'Alexis' && password === '12345678')
    return done(null,{id:1, name:'Alexis Cadrasco García'});
    done(null,false);
}));

//serialización y deserialización

passport.serializeUser(function(user,done){
    done(null,user.id);
});

passport.deserializeUser(function(id,done){
    done(null,{id:1, name:'Alexis Cadrasco García'});
});

app.set('view engine', 'ejs');

app.get('/',(req,res,next)=>{
    if(req.isAuthenticated()) return next();
    res.redirect('/login');
},(req,res)=>{
    console.log('Bienvenido al sistema....');
    res.render('bienvenido');
});

app.get('/login',(req,res)=>{
    res.render('login');
});

app.post('/login', passport.authenticate('local',{
    successRedirect:'/',
    failureRedirect:'/login'
}));

app.listen(9000,()=> console.log('server started'));