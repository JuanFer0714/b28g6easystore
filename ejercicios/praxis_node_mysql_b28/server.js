const express = require('express');
const mysql = require('mysql');
const conec = require('express-myconnection');

const routes = require('./router');

//settings
const app = express();
app.set('port', process.env.PORT || 9003);

//BD
const dbOptions = {
    host: 'db4free.net',
    port: 3306,
    user: 'easystore',
    password: 'easystore',
    database: 'easystoreb28_g6'
}

// middlewares -------------------------------------
app.use(conec(mysql, dbOptions, 'single'));
app.use(express.json());

// routes -------------------------------------------

app.use('/api/resources', routes)

//correr mi servidor
app.listen(app.get('port'), ()=>{
    console.log('El servidor esta corriendo en el puerto',app.get('port') );
});