const express = require('express');
const routes = express.Router(); 

routes.get('/all', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM tienda', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});

routes.post('/add', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO tienda set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('user added!');
        });
    });
});

routes.delete('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM tienda WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('user excluded!');
        });
    });
});


routes.put('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE tienda set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('user updated!');
        });
    });
});


module.exports = routes;