const mongoose = require('mongoose');
const User = require('../models/User');

//Listar usuarios

const findAllUsers = (req, res) => {
    User.find((err, users) => {
        err && res.sendStatus(500).send(err.message);

        res.status(200).json(users);
    });
}

//Listar id

const findById = (req, res) =>{
    User.findById(req.params.id,(err, user) =>{
        err && res.sendStatus(500).send(err.message);

        res.status(200).json(user);
    });
}

//Agregar usuario

const addUser = (req, res) =>{
    let user = new User({
        name: req.body.name,
        username: req.body.username,
        password: req.body.password
    });

    user.save((err,usr)=>{
        err && res.sendStatus(500).send(err.message);

        res.status(200).json(user);
    });

}

//Eliminar 
const deleteUser =(req, res) =>{
    User.findByIdAndDelete(req.params.id, (err,user) =>{

        err && res.sendStatus(500).send(err.message);

        res.status(200).json(user);
    });
}

//Actualizar

const updateUser = (req,res) => {

    User.findOneAndUpdate(req.params.id,{$set:req.body},{new:true},(err,user)=>{
            err && res.sendStatus(500).send(err.message);

            res.status(200).json(user);
    });   
}

//Exportar

module.exports = {findAllUsers,findById,addUser,deleteUser,updateUser};