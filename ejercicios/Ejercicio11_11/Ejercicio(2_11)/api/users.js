const UserControllers = require('../controller/users');
const express = require('express');

const router = express.Router();

router.get("/all", UserControllers.findAllUsers);

router.get("/:id", UserControllers.findById);

router.post("/add", UserControllers.addUser);

router.delete("/:id", UserControllers.deleteUser);

router.put("/:id", UserControllers.updateUser);

//Exportar

module.exports = router
