const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Crear esquema
const UserSchema = new Schema({
    name: {type: String},
    username: {type: String},
    password: {type: String}
});

//Exportar
module.exports = User = mongoose.model('User', UserSchema);