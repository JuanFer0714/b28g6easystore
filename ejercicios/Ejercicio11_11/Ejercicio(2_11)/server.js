const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./api/users');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));

app.use(bodyParser.json());

//End point
app.use('/api/users', User);

//conectar bd

mongoose.connect(
    "mongodb://localhost/practica2_11",
    {useNewUrlParser: true},
    (err, req) => {
        err && console.log('Error conectando a la bd');

        app.listen(4000,() =>{
            console.log('Servidor corriendo en el http://localhost:4000');
        });
    }
)