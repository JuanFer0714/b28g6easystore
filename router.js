const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const routes = express.Router();

routes.delete('/trabajador/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM trabajador WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('trabajador eliminado');
        });
    });
});

routes.delete('/producto/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM producto WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('producto eliminado');
        });
    });
});

routes.delete('/categoria/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM categoria WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('categoria eliminada');
        });
    });
});

routes.delete('/marca/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM marca WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('marca eliminada');
        });
    });
});

routes.delete('/provedor/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('DELETE FROM provedor WHERE id = ?', [req.params.id], (err, rows)=>{
            if(err) return res.send(err)

            res.send('provedor eliminado');
        });
    });
});


//---------------------------------------------------------------------


routes.post('/tienda', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO tienda set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('tienda agregada');
        });
    });
});

routes.post('/usuario', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO usuarios set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('tienda agregada');
        });
    });
});

routes.post('/trabajador', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO trabajador set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('trabajador agregado');
        });
    });
});

routes.post('/categoria', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO categoria set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('categoria agregada');
        });
    });
});

routes.post('/producto', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO producto set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('producto agregado');
        });
    });
});

routes.post('/marca', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO marca set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('marca agregada');
        });
    });
});

routes.post('/provedor', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO provedor set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('provedor agregado');
        });
    });
});

routes.post('/compra', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('INSERT INTO compra set ?', [req.body], (err, rows)=>{
            if(err) return res.send(err);

            res.send('compra agregada');
        });
    });
});


//---------------------------------------------------------------------


routes.get('/trabajador/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM trabajador where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/producto/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM producto where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/marca/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM marca where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/provedor/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM provedor where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/categoria/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM categoria where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/compra/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM compra where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/venta/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM venta where id = ?',[req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});


//---------------------------------------------------------------------


routes.get('/trabajadores', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM trabajador', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/productos', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM producto', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/marcas', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM marca', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/provedores', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM provedor', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/categorias', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM categoria', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/compras', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM compra', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
routes.get('/ventas', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);

        conn.query('SELECT * FROM venta', (err, rows)=>{
            if(err) return res.send(err);

            res.json(rows);
        });
    });
});
//---------------------------------------------------------------------
routes.put('/tienda/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE tienda set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('tienda actualizada!');
        });
    });
});

routes.put('/trabajador/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE trabajador set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('trabajador actualizado');
        });
    });
});

routes.put('/marca/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE marca set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('marca actualizada');
        });
    });
});

routes.put('/provedor/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE provedor set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('provedor actualizado');
        });
    });
});

routes.put('/producto/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE producto set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('producto actualizado');
        });
    });
});

routes.put('/categoria/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if(err) return res.send(err);
        conn.query('UPDATE categoria set ? WHERE id = ?', [req.body, req.params.id], (err, rows)=>{
            if(err) return res.send(err);

            res.send('categoria actualizada');
        });
    });
});


module.exports = routes;