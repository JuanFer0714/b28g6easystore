const express = require('express');
const mysql = require('mysql');
const con = require('express-myconnection');
const routes = require('./router');

const app = express();
app.set('port', process.env.PORT || 8000);
app.use(express.urlencoded({extended:false}));
app.use(express.json());

const dbOptions = {
    host: 'db4free.net',
    port: 3306,
    user: 'easystore',
    password: 'easystore',
    database: 'easystoreb28_g6'
}

// middlewares -------------------------------------
app.use(con(mysql, dbOptions, 'single' ));

//router
app.use('/api/resources', routes);

//server---------------------------------
app.listen(app.get('port'), ()=>{
    console.log("Servidor corriendo en el puerto ", app.get('port'));
});


