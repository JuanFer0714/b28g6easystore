const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const routes = express.Router();

//--------------------------- LOGIN AND REGISTRE -------------------------

routes.post('/login', async(req, res) => {
    req.getConnection((err, conn) => {
        if (err) return res.send(err);
        let user = req.body.username;
        let passw = req.body.password;
        conn.query('SELECT * FROM usuarios where username = ?', [user], (err, rows) => {
            if (err) return res.send(err);
            let hashsave = rows[0].password
            bcrypt.compare(passw.toString(), hashsave, (err, coinciden) => {
                if (err) {
                    console.log("Error comprobando", err);
                } else {
                    console.log("Complety: " + coinciden);
                    if (coinciden) {
                        jwt.sign({ user: rows }, 'secretkey', (err, token) => {
                            res.json({
                                token: token,
                                expiresIn: '7d'
                            });
                        });
                    } else {
                        res.json({
                            message: "Las credenciales no coinciden"
                        });
                    }
                }
            });
        });
    });
});

routes.post('/registrouser', (req, res) => {
    req.getConnection((err, conn) => {
        if (err) return res.send(err);

        var salt = bcrypt.genSaltSync(10);
        req.body.password = bcrypt.hashSync(req.body.password.toString(), salt);
        console.log(req.body);
        conn.query('INSERT INTO usuarios set ?', [req.body], (err, rows) => {
            if (err) return res.send(err);
            res.send('User added!');
        })
    })
});

routes.post('/registrotienda', (req, res) => {
    req.getConnection((err, conn) => {
        if (err) return res.send(err);
        console.log(req.body);
        conn.query('INSERT INTO tienda set ?', [req.body], (err, rows) => {
            if (err) return res.send(err);
            res.send('Shop added!');
        })
    })
});

//-------------------------------------------------------------------
routes.delete('/trabajador/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('DELETE FROM trabajador where trabajador_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('trabajador eliminado')
                });
            });
        }
    });
});

routes.delete('/producto/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('DELETE FROM producto where producto_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('producto eliminado')
                });
            });
        }
    });
});

routes.delete('/categoria/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('DELETE FROM categoria where categoria_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('categoria eliminada')
                });
            });
        }
    });
});

routes.delete('/marca/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('DELETE FROM marca where marca_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('marca eliminada')
                });
            });
        }
    });
});

routes.delete('/provedor/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('DELETE FROM provedor where provedor_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('provedor eliminado')
                });
            });
        }
    });
});


//---------------------------------------------------------------------
routes.post('/trabajador', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO trabajador set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('trabajador agregado')
                });
            });
        }
    });
});

routes.post('/categoria', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO categoria set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('categoria agregada')
                });
            });
        }
    });
});

routes.post('/producto', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO producto set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('producto agregado')
                });
            });
        }
    });
});

routes.post('/marca', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO marca set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('marca agregada')
                });
            });
        }
    });
});

routes.post('/provedor', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO provedor set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('provedor agregado')
                });
            });
        }
    });
});

routes.post('/compra', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO compra set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('compra agregada')
                });
            });
        }
    });
});

routes.post('/venta', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('INSERT INTO venta set ?', [req.body], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('venta agregada')
                });
            });
        }
    });
});


//---------------------------------------------------------------------


routes.get('/tienda/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM tienda where tienda_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})

routes.get('/producto/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM producto where producto_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})
routes.get('/marca/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM marca where marca_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})
routes.get('/provedor/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM provedor where provedor_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})
routes.get('/categoria/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM categoria where categoria_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})
routes.get('/compra/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM compra where compra_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})
routes.get('/venta/:id', verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)
                conn.query('SELECT * FROM venta where venta_id = ?', [req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
})


//---------------------------------------------------------------------


routes.get('/trabajadores', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM trabajador', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});

routes.get('/productos', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM producto', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
routes.get('/marcas', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM marca', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
routes.get('/provedores', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM provedor', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
routes.get('/categorias', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM categoria', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
routes.get('/compras', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM compra', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
routes.get('/ventas', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err)

                conn.query('SELECT * FROM venta', (err, rows) => {
                    if (err) return res.send(err)

                    res.json(rows)
                });
            });
        }
    });
});
//---------------------------------------------------------------------
routes.put('/usuario/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);

                var salt = bcrypt.genSaltSync(10);
                req.body.password = bcrypt.hashSync(req.body.password.toString(), salt);

                console.log(req.body);
                conn.query('UPDATE usuarios set ? where user_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Usuario actualizado!')
                })
            })
        }
    });
});

routes.put('/tienda/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE tienda set ? where tienda_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Tienda actualizada!')
                })
            })
        }
    });
});

routes.put('/trabajador/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE trabajador set ? where trabajador_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Trabajador actualizado!')
                })
            })
        }
    });
});

routes.put('/marca/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE marca set ? where marca_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Marca actualizada!')
                })
            })
        }
    });
});

routes.put('/provedor/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE provedor set ? where provedor_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Provedor actualizado!')
                })
            })
        }
    });
});

routes.put('/producto/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE producto set ? where producto_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Producto actualizado!')
                })
            })
        }
    });
});

routes.put('/categoria/:id', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            req.getConnection((err, conn) => {
                if (err) return res.send(err);
                conn.query('UPDATE categoria set ? where categoria_id = ?', [req.body, req.params.id], (err, rows) => {
                    if (err) return res.send(err)

                    res.send('Categoria actualizada!')
                })
            })
        }
    });
});

routes.post('/posts', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                message: 'post fue creado',
                authData
            });
        }
    });
});

//function <barer> token
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}

module.exports = routes;